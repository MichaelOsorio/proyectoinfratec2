﻿// isis1304-111-proyecto2.cpp: define el punto de entrada de la aplicaci—n de consola.
//
// DESARROLLADO POR:
// Juliana Jaime, 201224513
// Harold Gonzalez, 201213646
// Nombre, carnet

#define _CRT_SECURE_NO_DEPRECATE
#include "stdlib.h"
#include "string.h"
#include "stdio.h"


// La representacion de la imagen
typedef struct img
{
	int ancho;
	int alto;
	unsigned char *informacion;
} Imagen;


// Funci—n que carga el bmp en la estructura Imagen
void cargarBMP24(Imagen * imagen, char * nomArchivoEntrada);

// Funcion que guarda el contenido de la estructura imagen en un archivo binario
void guardarBMP24(Imagen * imagen, char * nomArchivoSalida);

void decidir(char nomArch1[], char op, int bitsPorByte, char arg4[], Imagen * img);

//Funcion que inserta un mensaje en la imagen usando n bits por Byte
void insertarMensaje(Imagen * img, char mensaje[], int n);

//Funcion que lee un mensaje de una imagen dando la longitud del mensaje y el numero de bits por byte usados
void leerMensaje(Imagen * img, char msg[], int l, int n);

unsigned char sacarNbits(char mensaje[], int bitPos, int n);

// Programa principal
// NO MODIFICAR
int main(int argc, char* argv[]) {
	Imagen *img = (Imagen *)malloc(sizeof(Imagen));
	char arg4[10000] = "";
	char op;
	int bitsPorByte;
	int i;
	int n;
	char nomArch1[256] = "";

	if (argc != 5) {
		printf("Faltan argumentos\n");
		system("pause");
		return -1;
	}

	strcat(nomArch1, argv[1]);
	op = argv[2][0];
	sscanf(argv[3], "%d", &bitsPorByte);
	strcpy(arg4, argv[4]);

	printf("op: %c\n", op);
	printf("bits per Byte: %d\n", bitsPorByte);
	printf("arg4: %s\n", arg4);

	// Cargar los datos
	cargarBMP24(img, nomArch1);

	decidir(nomArch1, op, bitsPorByte, arg4, img);
	system("pause");
	return 0;
}

/**
* Identifica la opci—n ingresada y realiza las acciones que debe hacer el programa.
* Ver el enunciado.
*/
// ESCRIBIR EN ENSAMBLADOR, SE PUEDEN USAR NOMBRES SIMBOLICOS
void decidir(char nomArch1[], char op, int bitsPorByte, char msg[], Imagen * img) {
	__asm {
		mov eax, op;         //Se mueve op al registro eax
		cmp eax, 114;		 //Se compara con el valor en ascii de 'r'
		je leer;				//Si es igual salta a la etiqueta leer
		cmp eax, 119;			//Si no, se compara con el valor ascii de 'w'
		je insertar;		//Si es igual salta a la etiqueta isnertar
		
	insertar:
		push nomArch1;
		push bitsPorByte;
		push msg;
		push img;
		call insertarMensaje;
		jmp fin;
	leer:
		push nomArch1;
		push bitsPorByte;
		push msg;
		push img;
		call leerMensaje;

	fin:

	}
}

/**
* Inserta un mensaje, de a n bits por componente de color, en la imagen apuntada por img
* parametro img Apuntador a una imagen en cuyos pixeles se almacenará el mensaje.
* parametro mensaje Apuntador a una cadena de caracteres con el mensaje.
* parametro n Cantidad de bits del mensaje que se almacenarán en cada componente de color de cada pixel. 0 < n <= 8.
*/
// ESCRIBIR EN ENSAMBLADOR, *NO* SE PUEDEN USAR NOMBRES SIMBOLICOS
void insertarMensaje(Imagen * img, char mensaje[], int n) {
	__asm {
		push ebp
		mov ebp, esp
		sub esp, 32
		push ebx				//parametro img
		push ecx				
		push edi				//parametro n
		mov ebx, [ebp + 8]		
		mov bl, [ebx]			
		mov ch, [ebp + 12]		//parametro mensaje
		mov edi, [ebp + 16]		
		mov esi, [ebp - 20]		// variable blocks
		mov esi, 8
		idiv esi, [edi]
		mov edx, [ebp - 28]		//variable listpix
		mov dl, [edx]
		mov dl, bl
		mov [ebp - 8], 0		//variable x
		
		for1:					//for1
		cmp [ebp - 8] , 0
		je finFor1
		cmp [ebp - 8], ch
		ja finFor1
		mov ch, dh				//carAct= mensaje
		
		if1:					//if1
		mov eax, 0				//res = 0
		jne else1
		cpm eax , 0
		jne else1
		mov [ebp - 16], 0		//variable y
		inc [ebp - 8]
		
		for2:					//for2
		cmp [ebp - 16], 0
		jne finFor2
		cmp [ebp - 16], esi
		ja finFor2
		mov cl, dh
		imul edi, [ebp -16]   //n*y
		mov ah, [edi]
		SHL cl , ah
		sub edi, 8
		neg edi
		SHR cl, ah
		mov al, dl
		SHR al, ah
		SHL al, ah 
		or al , dh
		mov edx, al
		inc edx
		mov cl, [ebp - 16]
		inc cl
		jmp for2
		
		else1:				//else1
		sub edi, eax
		mov ecx, edi //paste
		mov cl, dh  //carAux
		sub dh, 1
		mov ah, dh //carAnt
		sub eax, 8
		neg eax
		mov al, [eax]
		SHL ah , al
		SHR ah,  al
		mov ch, [ecx]
		SHL ah, ch
		sub ecx, 8
		neg ecx
		SHR ah, ch
		or ah, cl
		mov al, dl  //carMod
		mov bh,0
		mov bh, [edi]
		SHR al, bh				
		SHL al, bh				
		or al, cl
		mov dl, al
		inc dl
		add eax, 8
		cdq
		idiv edi
		mov eax, edi
		jmp if2
	
		if2:				//if2
		sub ecx, 8
		neg ecx
		cmp ecx, edi
		jb if3
		
		for3:			//for3
		mov ah, 0
		mov ah, [ebp -16]
		cmp ah, esi
		ja finFor3
		mov cl, dh        //carAux
		imul edi, ah
		add edi, ecx
		mov bh, 0
		mov bh, [edi]
		SHL cl, bh            
		sub edi, 8
		neg edi
		mov al, dl
		SHR al , bh			
		SHL al, bh			
		or al, cl
		mov dl, al
		inc dl
		inc ah
		jmp for3
		
		finFor3:			//finFor3
		jmp if3
		
		finFor2:			//finFor2
		mov eax, 8
		cdq
		idiv edi
		jmp if1
		
		if3:				//if3
		mov ah, 0
		mov ah, [ebp -8]
		sub ch, 1
		cmp ah, ch
		jne finElse
		cmp eax, 0
		je finElse
		sub edi, eax
		mov ecx, edi		 //paste
		mov cl, 0
		mov cl, dh		//carAux
		sub eax, 8
		neg eax
		mov bh, [eax]
		SHL cl, bh
		SHR cl, bh
		mov bl, 0
		mov bl, [ecx]
		SHL cl, bl
		mov al, 0
		mov al, dl
		mov bh, 0
		mov bh, [edi]
		SHR al, bh		
		SHL al, bh      
		or al, cl
		mov dl, al
		inc dl
		jmp finElse
		
		finElse:				//finElse
		inc [ebp - 8]
		jmp finFor1
		
		finFor1:				//finFor1
		pop edi
		pop ecx
		pop ebx
		add esp, 32
		pop ebx

	}
}

/**
* Extrae un mensaje de tama–o l, guardado de a n bits por componente de color, de la imagen apuntada por img
* parametro img Apuntador a una imagen que tiene almacenado el mensaje en sus pixeles.
* parametro msg Apuntador a una cadena de caracteres donde se depositará el mensaje.
* parametro l Tama–o en bytes del mensaje almacenado en la imagen.
* parametro n Cantidad de bits del mensaje que se almacenan en cada componente de color de cada pixel. 0 < n <= 8.
*/
// ESCRIBIR EN ENSAMBLADOR, SE PUEDEN USAR NOMBRES SIMBOLICOS
void leerMensaje(Imagen * img, char msg[], int l, int n) {
	//Pixeles de la imagen
	unsigned char * pixeles = img->informacion;

	//Pixel actual
	unsigned char pActual;

	//Guarda informacion pixel actual
	unsigned char pAux;


	//Representa cantidad bits leidos por byte !>8
	int readBits = 0;

	//Representa cantidad bits por leer de un byte 
	int restBits = 0;


	//Representa indice recorrido a trav�s de pixeles de la imagen por parametro
	int x = 0;


	//Posicion caracter actual mensaje a leer
	int caracterActual = 0;

	//Pixel anterior al actual
	unsigned char pAnterior;

	//Representa caracter donde se agregan bits leidos
	unsigned char bitsAgregados = '\0';

	__asm {
		//recorrido de la imagen
		mov esi, 0; //contador para el for int x =0;

	recorrer: //{
		mov ebx, l; //Se lleva el valor de l al registro eax
		imul ebx, 10; // se multploca el valor de l por 10
		cmp ebx, esi;
		//if(x == l*10)
		je finRecorrer;



		mov ecx, restBits; //Se trae los bits restantes
		cmp ecx, 0;
		//if{ restBits == 0;

		jne restNoCero;

		mov edx, pixeles[esi]; //Se trae la dirección del pixel en la posición [x]
		mov ebx, 0;
		mov  bl, [edx]; // se trae el pixel en la pos x. bl = pActual;
		mov pActual, bl; // se asigna el pixel a pActual;
		mov ecx, n; //traer el valor de n
		sub ecx, 8; // hacer n-8
		neg ecx; //negar n-8 para que quede 8-n
		SHL pActual, cl; // realizar el corrimiento pActual << (8-n)
		mov bl, pActual; // se trae pActual ya corrido
		mov pAux, bl; // se asigna pActual ya corrido a pAux. pAux = pActual << (8-n)
		mov ecx, readBits; // Se trae el valor de readBits
						   //mov ebx, 0;
						   //mov bl, pAux;
		SHR pAux, cl; // se hace pAux >> readBits
					  //mov pAux, bl; // Se hace pAux = pAux >> readBits;
		mov bl, pAux;
		OR bitsAgregados, bl; // se hace bitsAgregados = (bitsAgregados|pAux)
		add ecx, n; // readBits + n
		mov readBits, ecx; // se realiza readBits = n;


						   // no se ejecuta el else;
		jmp saltarElse
			//}


			//else {
			restNoCero :
		mov ebx, readBits; // se trae el valor de readBits;
		cmp ebx, 0;

		// if{ readBits == 0;
		jne leerBitsPixelActual; // Se verifica que readBits !=0 y se salta

		dec esi;
		mov edx, pixeles[esi];//Se trae el pixel en la posición x-1
		inc esi;
		mov ebx, 0;
		mov bl, [edx]; // se hace pAnterior = pixeles[x-1]
		mov pAnterior, bl;
		mov ecx, restBits;
		sub ecx, 8;// se hace restBits -8
		neg ecx; // se convierte la resta anterior en 8-restBits
		SHL pAnterior, cl; // se realiza el corrimiento de pAnterior << 8-restBits;
						   //mov pAnterior, dl; //se mueve el valor ya corrido a pAnterior;
		OR bitsAgregados, dl; // se hace  bitsAgregados = (bitsAgragados|pAnterior)
		mov ecx, restBits; // se trae el valor original de restBits
		add readBits, ecx; // se realiza readBits += restBits
						   //}

	leerBitsPixelActual:
		mov edx, pixeles[esi]; //se trae la dirección del pixel en la posición [x];
		mov bl, [edx];
		mov pActual, bl;
		mov ecx, n; // se trae el valor de n
		sub ecx, 8; // se hace n -8 
		neg ecx; // se niega la operación para tener 8-n;
		SHL pActual, cl; // Se hace pActual = pActual << (8-n)
		mov bl, pActual; // se mueve pActual a un registro;
		mov pAux, bl; // se asigna pAux = pActual;
		mov ecx, readBits; // se trae el valor de readBits
		SHR pAux, cl; //  se jace pAux = pAux >> readBits;
		mov bl, pAux; // se trae el valor de pAux;
		OR bitsAgregados, bl; // se hace bitsAgregados = (bitsAgregados|pAux);
		mov ecx, n; // se trae el valor de n;
		add readBits, ecx; // se hace readBits += n;
						   //}
	saltarElse:
		//if(readBits == 8){

		mov eax, readBits; // se trae el valor de readBits;
		cmp eax, 8; // se compara readBits == 8
		jne diferente; // si no son iguales salta;
		mov dl, bitsAgregados; // se trae el valor de bitsAgregados;
		mov ebx, msg[caracterActual]; // se trae la dirección del mensaje en la pos del catacterActual;
		mov[ebx], dl; // se inserta el valor de bitsAgregados a donde apunta msg[catacterActual] ;
		inc caracterActual; // se hace caracterActual++
		mov dl, '\0'; // se mueve el caracter nulo
		mov bitsAgregados, dl; // se asigna a bitsAgregados el caracter nulo
		mov ecx, 0; // se hace ecx = 0;
		mov readBits, ecx; // se hacer readBits =0;
		mov restBits, ecx; // se hace restBits =0;
		jmp saltarDiferente;
		//}

		//else if(readBits > 8){ 
	diferente:

		cmp eax, 8;
		jl saltarDiferente;

		mov dl, bitsAgregados; // se trae el valor de bitsAgregados;
		mov ebx, msg[caracterActual]; // se trae la dirección del mensaje en la pos del catacterActual;
		mov[ebx], dl; // se inserta el valor de bitsAgregados a donde apunta msg[catacterActual] ;
		inc caracterActual; // se hace caracterActual++
		mov dl, '\0'; // se mueve el caracter nulo
		mov bitsAgregados, dl; // se asigna a bitsAgregados el caracter nulo
		mov ecx, readBits; // se trae el valor que readBits;
		sub ecx, 8; // se hacer readBits -8;
		mov restBits, ecx; // se hace restBits = readBits -8;
		mov ecx, 0; // se hace ecx = 0;
		mov readBits, ecx; // se hace readBits = ecx;
						   //}
						   //if(caracterActual == (l-1)){
	saltarDiferente:
		mov ebx, l; //Se lleva el valor de l al registro eax
		sub ebx, 1; // se hace l-1;
		cmp caracterActual, ebx; // se compara caracterActual == (l-1)
		je finRecorrer; // si son iguales se termina el ciclo;
						//}
		inc esi; // x++
		jmp recorrer;
		//}

	finRecorrer:
		//{
		//ciclo terminado;
		//}			
	}
}

/**
* Extrae n bits a partir del bit que se encuentra en la posici—n bitpos en la secuencia de bytes que
* se pasan como parámetro
* parametro secuencia Apuntador a una secuencia de bytes.
* parametro n Cantidad de bits que se desea extraer. 0 < n <= 8.
* parametro bitpos Posición del bit desde donde se extraerán los bits. 0 <= n < 8*longitud de la secuencia
* retorno Los n bits solicitados almacenados en los bits menos significativos de un unsigned char
*/
// PROCEDIMIENTO OPCIONAL
unsigned char sacarNbits(char mensaje[], int bitpos, int n) {
	// TODO
	__asm {

	}
}

// Lee un archivo en formato BMP y lo almacena en la estructura img
// NO MODIFICAR
void cargarBMP24(Imagen * imagen, char * nomArchivoEntrada) {
	// bmpDataOffset almacena la posici—n inicial de los datos de la imagen. Las otras almacenan el alto y el ancho
	// en pixeles respectivamente
	int bmpDataOffset, bmpHeight, bmpWidth;
	int y;
	int x;
	int	residuo;

	FILE *bitmapFile;
	bitmapFile = fopen(nomArchivoEntrada, "rb");
	if (bitmapFile == NULL) {
		printf("No ha sido posible cargar el archivo: %s\n", nomArchivoEntrada);
		exit(-1);
	}

	fseek(bitmapFile, 10, SEEK_SET); // 10 es la posici—n del campo "Bitmap Data Offset" del bmp
	fread(&bmpDataOffset, sizeof(int), 1, bitmapFile);

	fseek(bitmapFile, 18, SEEK_SET); // 18 es la posici—n del campo "height" del bmp
	fread(&bmpWidth, sizeof(int), 1, bitmapFile);
	bmpWidth = bmpWidth * 3;

	fseek(bitmapFile, 22, SEEK_SET); // 22 es la posici—n del campo "width" del bmp
	fread(&bmpHeight, sizeof(int), 1, bitmapFile);
	//bmpHeight = -bmpHeight; // Se multiplica 1 para convertirlo en valor positivo

	residuo = (4 - (bmpWidth) % 4) & 3; // Se debe calcular los bits residuales del bmp, que surjen al almacenar en palabras de 32 bits

	imagen->ancho = bmpWidth;
	imagen->alto = bmpHeight;
	imagen->informacion = (unsigned char *)calloc(bmpWidth * bmpHeight, sizeof(unsigned char));

	fseek(bitmapFile, bmpDataOffset, SEEK_SET); // Se ubica el puntero del archivo al comienzo de los datos

	for (y = 0; y < bmpHeight; y++) {
		for (x = 0; x < bmpWidth; x++) {
			int pos = y * bmpWidth + x;
			fread(&imagen->informacion[pos], sizeof(unsigned char), 1, bitmapFile);
		}
		fseek(bitmapFile, residuo, SEEK_CUR); // Se omite el residuo en los datos
	}
	fclose(bitmapFile);
}

// Esta funcion se encarga de guardar una estructura de Imagen con formato de 24 bits (formato destino) en un archivo binario
// con formato BMP de Windows.
// NO MODIFICAR
void guardarBMP24(Imagen * imagen, char * nomArchivoSalida) {
	unsigned char bfType[2];
	unsigned int bfSize, bfReserved, bfOffBits, biSize, biWidth, biHeight, biCompression, biSizeImage, biXPelsPerMeter, biYPelsPerMeter, biClrUsed, biClrImportant;
	unsigned short biPlanes, biBitCount;
	FILE * archivoSalida;
	int y, x;
	int relleno = 0;

	int residuo = (4 - (imagen->ancho) % 4) & 3; // Se debe calcular los bits residuales del bmp, que quedan al forzar en palabras de 32 bits


	bfType[2];       // Tipo de Bitmap
	bfType[0] = 'B';
	bfType[1] = 'M';
	bfSize = 54 + imagen->alto * ((imagen->ancho) / 3) * sizeof(unsigned char);       // Tamanio total del archivo en bytes
	bfReserved = 0;   // Reservado para uso no especificados
	bfOffBits = 54;    // Tamanio total del encabezado
	biSize = 40;      // Tamanio del encabezado de informacion del bitmap
	biWidth = (imagen->ancho) / 3;     // Ancho en pixeles del bitmap
	biHeight = imagen->alto;    // Alto en pixeles del bitmap
	biPlanes = 1;    // Numero de planos
	biBitCount = 24;  // Bits por pixel (1,4,8,16,24 or 32)
	biCompression = 0;   // Tipo de compresion
	biSizeImage = imagen->alto * imagen->ancho;   // Tamanio de la imagen (sin ecabezado) en bits
	biXPelsPerMeter = 2835; // Resolucion del display objetivo en coordenada x
	biYPelsPerMeter = 2835; // Resolucion del display objetivo en coordenada y
	biClrUsed = 0;       // Numero de colores usados (solo para bitmaps con paleta)
	biClrImportant = 0;  // Numero de colores importantes (solo para bitmaps con paleta)

	archivoSalida = fopen(nomArchivoSalida, "w+b"); // Archivo donde se va a escribir el bitmap
	if (archivoSalida == 0) {
		printf("No ha sido posible crear el archivo: %s\n", nomArchivoSalida);
		exit(-1);
	}

	fwrite(bfType, sizeof(char), 2, archivoSalida); // Se debe escribir todo el encabezado en el archivo. En total 54 bytes.
	fwrite(&bfSize, sizeof(int), 1, archivoSalida);
	fwrite(&bfReserved, sizeof(int), 1, archivoSalida);
	fwrite(&bfOffBits, sizeof(int), 1, archivoSalida);
	fwrite(&biSize, sizeof(int), 1, archivoSalida);
	fwrite(&biWidth, sizeof(int), 1, archivoSalida);
	fwrite(&biHeight, sizeof(int), 1, archivoSalida);
	fwrite(&biPlanes, sizeof(short), 1, archivoSalida);
	fwrite(&biBitCount, sizeof(short), 1, archivoSalida);
	fwrite(&biCompression, sizeof(int), 1, archivoSalida);
	fwrite(&biSizeImage, sizeof(int), 1, archivoSalida);
	fwrite(&biXPelsPerMeter, sizeof(int), 1, archivoSalida);
	fwrite(&biYPelsPerMeter, sizeof(int), 1, archivoSalida);
	fwrite(&biClrUsed, sizeof(int), 1, archivoSalida);
	fwrite(&biClrImportant, sizeof(int), 1, archivoSalida);

	// Se escriben en el archivo los datos RGB de la imagen.
	for (y = 0; y < imagen->alto; y++) {
		for (x = 0; x < imagen->ancho; x++) {
			int pos = y * imagen->ancho + x;
			fwrite(&imagen->informacion[pos], sizeof(unsigned char), 1, archivoSalida);
		}
		fwrite(&relleno, sizeof(unsigned char), residuo, archivoSalida);
	}
	fclose(archivoSalida);
}